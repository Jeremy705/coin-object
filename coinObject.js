const coin = {
    state: 0,
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        this.state = Math.floor(Math.random() * 2)
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
        if(this.state === 0){
            return "Heads"
        }else{
            return "Tails"
        }
    },
    toHTML: function() {
        const image = document.createElement('img');
        if (this.state === 0){
            image.src = "./images/heads.jpg"
        }else{
            image.src = "./images/tails.jpg"
        } 
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        return image;
    }
};

function display20Flips() {
    const results = [];
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
    for(let i = 0; i<20; i++){
        coin.flip()
        coin.toString()
        let result = coin.toString()
        results.push(result)
    }
    document.body.innerHTML = results.join("<br>");
    
}

display20Flips()


function display20Images() {
    let heads = document.createElement('div')
    heads.innerHTML = '<img class="coin" src="./images/heads.jpg">'
    let tails = document.createElement('div') 
    tails.innerHTML = '<img class="coin" src="./images/tails.jpg">'
    const results = [];
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
    
    for (let i = 0; i<20; i++){
        coin.flip()
        let result = coin.toString()
        results.push(result)
        console.log(coin.toHTML())
    }
    for(let i = 0; i <=results.length; i++){
        if(results[i] === "Heads"){
            document.body.appendChild(heads)
        }else{
            document.body.appendChild(tails)
        }
    }
    
}
display20Images()